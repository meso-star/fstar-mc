! Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the StarMC library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include <rsys/rsys.inc>

#include <star/ssp.inc>

MODULE SMC_BINDING

! (1): Special warning about some API calls manipulating uint64_t C-side
! ---------------------------------------------------------------------
! As unsigned types do not exist Fortran-side, SSP binding makes use of C_INT64_T
! where C_UINT64_T (that doesn't exist) would have been the perfect match
! Doing this garantees the size matches, but exposes to overflows if used
! careless Fortran-side (a 'too big' uint64_t will be treated as negative)
! A workaround is to TRANSFER the bytes of these unsigned-stored-in-a-signed
! to int128 (see below)

USE ISO_C_BINDING, ONLY: C_FUNPTR, C_PTR, C_SIZE_T, C_INT64_T, C_DOUBLE
IMPLICIT NONE

! Generic type descriptor
 TYPE, BIND(C) :: SMC_TYPE
  TYPE(C_FUNPTR) :: CREATE
  TYPE(C_FUNPTR) :: DESTROY
  TYPE(C_FUNPTR) :: SET
  TYPE(C_FUNPTR) :: ZERO
  TYPE(C_FUNPTR) :: ADD
  TYPE(C_FUNPTR) :: SUB
  TYPE(C_FUNPTR) :: MUL
  TYPE(C_FUNPTR) :: IDIV
  TYPE(C_FUNPTR) :: SQRT
 END TYPE SMC_TYPE

! Define interface of CREATE routine.
ABSTRACT INTERFACE
FUNCTION CREATE_TYPE_FUN (allocator, ctx)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: ctx
  TYPE(C_PTR) :: CREATE_TYPE_FUN
  END FUNCTION CREATE_TYPE_FUN
END INTERFACE

! Define interface of DESTROY routine.
ABSTRACT INTERFACE
SUBROUTINE DESTROY_TYPE_FUN (allocator, data)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: data
  END SUBROUTINE DESTROY_TYPE_FUN
END INTERFACE

! Define interface of SET routine.
ABSTRACT INTERFACE
SUBROUTINE SET_TYPE_FUN (result, value)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: value
  END SUBROUTINE SET_TYPE_FUN
END INTERFACE

! Define interface of ZERO routine.
ABSTRACT INTERFACE
SUBROUTINE ZERO_TYPE_FUN (result)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  END SUBROUTINE ZERO_TYPE_FUN
END INTERFACE

! Define interface of ADD routine.
ABSTRACT INTERFACE
SUBROUTINE ADD_TYPE_FUN (result, op0, op1)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: op0
  TYPE(C_PTR), INTENT(IN), VALUE :: op1
  END SUBROUTINE ADD_TYPE_FUN
END INTERFACE

! Define interface of SUB routine.
ABSTRACT INTERFACE
SUBROUTINE SUB_TYPE_FUN (result, op0, op1)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: op0
  TYPE(C_PTR), INTENT(IN), VALUE :: op1
  END SUBROUTINE SUB_TYPE_FUN
END INTERFACE

! Define interface of MUL routine.
ABSTRACT INTERFACE
SUBROUTINE MUL_TYPE_FUN (result, op0, op1)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: op0
  TYPE(C_PTR), INTENT(IN), VALUE :: op1
  END SUBROUTINE MUL_TYPE_FUN
END INTERFACE

! Define interface of DIVI routine.
ABSTRACT INTERFACE
SUBROUTINE DIVI_TYPE_FUN (result, op0, op1)
  USE ISO_C_BINDING, ONLY: C_PTR, C_SIZE_T
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: op0
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: op1
  END SUBROUTINE DIVI_TYPE_FUN
END INTERFACE

! Define interface of SQRT routine.
ABSTRACT INTERFACE
SUBROUTINE SQRT_TYPE_FUN (result, value)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: value
  END SUBROUTINE SQRT_TYPE_FUN
END INTERFACE

! Type for raw integration, i.e. whithout estimator. Experiment results are
! simply accumulated
 TYPE, BIND(C) :: SMC_TYPE_ACCUM
  TYPE(C_FUNPTR) :: CREATE
  TYPE(C_FUNPTR) :: DESTROY
  TYPE(C_FUNPTR) :: CLEAR
  TYPE(C_FUNPTR) :: ADD
 END TYPE SMC_TYPE_ACCUM

! Define interface of CREATE routine.
ABSTRACT INTERFACE
FUNCTION CREATE_ACCUM_FUN (allocator, ctx)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: ctx
  TYPE(C_PTR) :: CREATE_ACCUM_FUN
  END FUNCTION CREATE_ACCUM_FUN
END INTERFACE

! Define interface of DESTROY routine.
ABSTRACT INTERFACE
SUBROUTINE DESTROY_ACCUM_FUN (allocator, accum)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: accum
  END SUBROUTINE DESTROY_ACCUM_FUN
END INTERFACE

! Define interface of CLEAR routine.
ABSTRACT INTERFACE
SUBROUTINE CLEAR_ACCUM_FUN (accum)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: accum
  END SUBROUTINE CLEAR_ACCUM_FUN
END INTERFACE

! Define interface of ADD routine.
ABSTRACT INTERFACE
SUBROUTINE ADD_ACCUM_FUN (result, op0, op1)
  USE ISO_C_BINDING, ONLY: C_PTR
  TYPE(C_PTR), INTENT(IN), VALUE :: result
  TYPE(C_PTR), INTENT(IN), VALUE :: op0
  TYPE(C_PTR), INTENT(IN), VALUE :: op1
  END SUBROUTINE ADD_ACCUM_FUN
END INTERFACE

TYPE(SMC_TYPE), BIND(C, NAME='SMC_TYPE_NULL'), TARGET :: SMC_TYPE_NULL

 TYPE, BIND(C) :: SMC_ESTIMATOR_STATUS
  TYPE(C_PTR) :: E ! Expected value
  TYPE(C_PTR) :: V ! Variance
  TYPE(C_PTR) :: SE ! Standard error, i.e. sqrt(V / N)
  TYPE(C_PTR) :: N ! Samples count
 END TYPE SMC_ESTIMATOR_STATUS

 TYPE, BIND(C) :: SMC_ACCUMULATOR_STATUS
  TYPE(C_PTR) :: VALUE ! Accumulated value
  INTEGER(C_SIZE_T) :: N ! Samples count
 END TYPE SMC_ACCUMULATOR_STATUS

 TYPE, BIND(C) :: SMC_INTEGRATOR
  TYPE(C_FUNPTR) :: INTEGRAND
  TYPE(C_PTR) :: TYPE
  INTEGER(C_SIZE_T) :: MAX_STEPS ! Maximum # of experiments
 END TYPE SMC_INTEGRATOR

! Define interface of INTEGRAND routine.
ABSTRACT INTERFACE
FUNCTION INTEGRAND_INTEGRATOR_FUN (value, rng, ctx)
  USE ISO_C_BINDING, ONLY: C_PTR
  USE SSP_BINDING
  TYPE(C_PTR), INTENT(IN), VALUE :: value
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: rng
  TYPE(C_PTR), INTENT(IN), VALUE :: ctx
  TYPE(C_PTR) :: CREATE_ACCUM_FUN
  END FUNCTION INTEGRAND_INTEGRATOR_FUN
END INTERFACE

TYPE(SMC_INTEGRATOR), BIND(C, NAME='SMC_INTEGRATOR_NULL'), TARGET :: SMC_INTEGRATOR_NULL

 TYPE, BIND(C) :: SMC_INTEGRATOR_ACCUM
  TYPE(C_FUNPTR) :: INTEGRAND
  TYPE(C_PTR) :: TYPE
  INTEGER(C_SIZE_T) :: MAX_STEPS ! Maximum # of experiments
 END TYPE SMC_INTEGRATOR_ACCUM

! Define interface of INTEGRAND routine.
ABSTRACT INTERFACE
FUNCTION INTEGRAND_INTEGRATOR_ACCUM_FUN (value, rng, ctx)
  USE ISO_C_BINDING, ONLY: C_PTR
  USE SSP_BINDING
  TYPE(C_PTR), INTENT(IN), VALUE :: value
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: rng
  TYPE(C_PTR), INTENT(IN), VALUE :: ctx
  TYPE(C_PTR) :: CREATE_ACCUM_FUN
  END FUNCTION INTEGRAND_INTEGRATOR_ACCUM_FUN
END INTERFACE

TYPE(SMC_INTEGRATOR_ACCUM), BIND(C, NAME='SMC_INTEGRATOR_ACCUM_NULL'), TARGET :: SMC_INTEGRATOR_ACCUM_NULL

TYPE(SMC_TYPE), BIND(C, NAME='smc_float'), TARGET :: SMC_FLOAT
TYPE(SMC_TYPE), BIND(C, NAME='smc_double'), TARGET :: SMC_DOUBLE

INTERFACE

!*******************************************************************************
! Device API
!*******************************************************************************

! The nthreads_hint parameter is uint C-side
! But ISO_C_BINDING limitations force to declare it as int
! To avoid overflows, this value should be manipulated through TRANSFER
! See (1) and Fortran examples
 FUNCTION smc_device_create(logger, allocator, nthreads_hint, type, dev) BIND(C, NAME='smc_device_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  USE SSP_BINDING
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: logger ! May be C_NULL_PTR <=> use default logger
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator ! May be C_NULL_PTR <=> use default allocator
  INTEGER(C_INT), INTENT(IN), VALUE :: nthreads_hint ! Hint on the number of threads to use
  TYPE(C_PTR), INTENT(IN), VALUE :: type ! May be NULL <=> use default RNG type
  TYPE(C_PTR), INTENT(OUT) :: dev
  INTEGER(C_INT) :: smc_device_create
 END FUNCTION smc_device_create

 FUNCTION smc_device_ref_get(dev) BIND(C, NAME='smc_device_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  INTEGER(C_INT) :: smc_device_ref_get
 END FUNCTION smc_device_ref_get

 FUNCTION smc_device_ref_put(dev) BIND(C, NAME='smc_device_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  INTEGER(C_INT) :: smc_device_ref_put
 END FUNCTION smc_device_ref_put

 FUNCTION smc_device_set_rng_type(dev, type) BIND(C, NAME='smc_device_set_rng_type')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  USE SSP_BINDING
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  INTEGER(C_INT) :: smc_device_set_rng_type
 END FUNCTION smc_device_set_rng_type

 FUNCTION smc_device_get_rng_type(dev, type) BIND(C, NAME='smc_device_get_rng_type')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  USE SSP_BINDING
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  INTEGER(C_INT) :: smc_device_get_rng_type
 END FUNCTION smc_device_get_rng_type

!*******************************************************************************
! Integration API
!*******************************************************************************

 FUNCTION smc_solve(dev, integrator, ctx, estimator) BIND(C, NAME='smc_solve')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SMC_INTEGRATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(SMC_INTEGRATOR), INTENT(IN) :: integrator
  TYPE(C_PTR), INTENT(IN), VALUE :: ctx
  TYPE(C_PTR), INTENT(IN),VALUE :: estimator
  INTEGER(C_INT) :: smc_solve
 END FUNCTION smc_solve

 FUNCTION smc_solve_N(dev, integrator, contexts, sizeof_context, estimators) BIND(C, NAME='smc_solve_N')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  IMPORT SMC_INTEGRATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(SMC_INTEGRATOR), INTENT(IN) :: integrator
  TYPE(C_PTR), INTENT(IN), VALUE :: contexts
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: sizeof_context
  TYPE(C_PTR), INTENT(IN) :: estimators(*)
  INTEGER(C_INT) :: smc_solve_N
 END FUNCTION smc_solve_N

 FUNCTION smc_estimator_ref_get(estimator) BIND(C, NAME='smc_estimator_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: estimator
  INTEGER(C_INT) :: smc_estimator_ref_get
 END FUNCTION smc_estimator_ref_get

 FUNCTION smc_estimator_ref_put(estimator) BIND(C, NAME='smc_estimator_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: estimator
  INTEGER(C_INT) :: smc_estimator_ref_put
 END FUNCTION smc_estimator_ref_put

 FUNCTION smc_estimator_get_status(estimator, status) BIND(C, NAME='smc_estimator_get_status')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SMC_ESTIMATOR_STATUS
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: estimator
  TYPE(SMC_ESTIMATOR_STATUS), INTENT(OUT) :: status
  INTEGER(C_INT) :: smc_estimator_get_status
 END FUNCTION smc_estimator_get_status

!*******************************************************************************
! Accumulation API
!*******************************************************************************

! Raw integration. The realisations must be accumulated by the user defined
! integrand
 FUNCTION smc_integrate(dev, integrator, ctx, accumulator) BIND(C, NAME='smc_integrate')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SMC_INTEGRATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(SMC_INTEGRATOR), INTENT(IN) :: integrator
  TYPE(C_PTR), INTENT(IN), VALUE :: ctx
  TYPE(C_PTR), INTENT(IN),VALUE :: accumulator
  INTEGER(C_INT) :: smc_integrate
 END FUNCTION smc_integrate

 FUNCTION smc_accumulator_ref_get(accumulator) BIND(C, NAME='smc_accumulator_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: accumulator
  INTEGER(C_INT) :: smc_accumulator_ref_get
 END FUNCTION smc_accumulator_ref_get

 FUNCTION smc_accumulator_ref_put(accumulator) BIND(C, NAME='smc_accumulator_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: accumulator
  INTEGER(C_INT) :: smc_accumulator_ref_put
 END FUNCTION smc_accumulator_ref_put

 FUNCTION smc_accumulator_get_status(accumulator, status) BIND(C, NAME='smc_accumulator_get_status')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SMC_ACCUMULATOR_STATUS
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: accumulator
  TYPE(SMC_ACCUMULATOR_STATUS), INTENT(OUT) :: status
  INTEGER(C_INT) :: smc_accumulator_get_status
 END FUNCTION smc_accumulator_get_status

END INTERFACE

END MODULE SMC_BINDING
