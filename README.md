# FStar Monte-Carlo

fStarMC provides Fortran users with a way to use the StarMC C library.
Most of the types and functions in StarMC are just made accessible through
Fortran's ISO_C_BINDING intrinsic module.

## Install

### Pre-requisites

fStarMC is compatible GNU/Linux as well as Microsoft Windows 7 and later, both
in 64-bits. 
It was successfully built with the [GNU Compiler Collection](https://gcc.gnu.org) 
(versions 4.7 and later), with the flags:
[-std=f2008ts](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(Fortran 2008 standard including the additions of TS 29113 and TS 18508), 
[-ffree-form](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(free form layout),
[-ffree-line-length-none](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(no limit on source file's line length), and
[-cpp](https://gcc.gnu.org/onlinedocs/gfortran/Preprocessing-Options.html)
(C-preprocessing enabled on source files).
No compiler was tested on Windows; it is assumed that Intel Fortran could be 
successfully used.
It relies on [CMake](http://www.cmake.org) and the 
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. 
It also depends on the [StarMC](https://gitlab.com/meso-star/star-mc/) library.

### How to build

First ensure that CMake and a C and a Fortran compiler with Fortran 2008 + 
TS 29113 support (i.e. gfortran 4.7 or later) are installed on your system.
Then install the [RCMake](https://gitlab.com/vaplv/rcmake.git) package as 
well as the [RSys](https://gitlab.com/vaplv/rsys.git) and the
[StarMC](https://gitlab.com/meso-star/star-mc/) libraries.
Finally Generate the project from the `cmake/CMakeLists.txt` file by appending 
to the `CMAKE_PREFIX_PATH` variable the `<RCMAKE_INSTALL_DIR>/lib/cmake`,
`<RSYS_INSTALL_DIR>` and `<SMC_INSTALL_DIR>` directories, where 
`<RCMAKE_INSTALL_DIR>`, `<RSYS_INSTALL_DIR>` and `<SMC_INSTALL_DIR>` are the 
install directories of the RCMake package and the RSys and StarMC libraries, 
respectively.
The resulting project can be edited, built, tested and installed as any CMake 
project (Refer to the [CMake documentation](https://cmake.org/documentation) 
for further informations on CMake).

Example on a GNU/Linux system:

    ~ $ git clone https://gitlab.com/meso-star/fstar-mc.git
    ~ $ mkdir star-mc/build && cd fstar-mc/build
    ~/fstar-mc/build $ cmake -G "Unix Makefiles" \
    > -DCMAKE_PREFIX_PATH="<RCMAKE_DIR>/lib/cmake;<RSYS_DIR>;<SMC_DIR>" \
    > -DCMAKE_INSTALL_PREFIX=<FMC_INSTALL_DIR> \
    > ../cmake
    ~/fstar-mc/build $ make && make test
    ~/fstar-mc/build $ make install

with `<FMC_INSTALL_DIR>` the directory in which fstar-mc is going to be
installed.

### How to use

Using fStarMC requires a fortran compiler supporting the Fortran 2008 standard
and TS 29113 addition, free form layout, unlimited line length for source 
code, and C-preprocessing of Fortran files.

When using gfortran, the adequate options are -std=f2008ts -ffree-form 
-ffree-line-length-none -cpp.

Provided include files (*.inc) must be included using #include, as the Fortran 
INCLUDE statement doesn't allow C-like preprocessor directives in included 
files.

The fmc companion library should be added at the link stage.

For an example of use, please look at the provided test programs.

## Licenses

fStarMC is Copyright (C) |Meso|Star> 2015 (<contact@meso-star.com>). It
is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.

